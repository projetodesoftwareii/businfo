<?php

class ParadaController extends GxController {

	public function accessRules() {
		return array(
			array('allow', 'groups' => array('usuarios')),
			array('deny', 'users' => array('*'))
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Parada'),
		));
	}

	public function actionCreate() {
		$model = new Parada;


		if (isset($_POST['Parada'])) {
			$model->setAttributes($_POST['Parada']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id_parada));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Parada');


		if (isset($_POST['Parada'])) {
			$model->setAttributes($_POST['Parada']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id_parada));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionPreDelete() {
		$dataProvider = Parada::model()->findAll();
		$this->render('delete', array(
			'model' 		=> new Parada(),
			'dataProvider' 	=> $dataProvider,
			'salvo'			=> FALSE,
		));
	}

	public function actionPosDelete() {
		$dataProvider = Parada::model()->findAll();
		$this->render('delete', array(
			'model' 		=> new Parada(),
			'dataProvider' 	=> $dataProvider,
			'salvo'			=> TRUE,
		));
	}	

	public function actionDelete($id) {
		$this->loadModel($id, 'Parada')->delete();
		$this->redirect(array('parada/posDelete'));
	}

	public function actionIndex() {
		$dataProvider = Parada::model()->findAll();
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Parada('search');
		$model->unsetAttributes();
		$dataProvider = Parada::model()->findAll();

		if (isset($_GET['Parada']))
			$model->setAttributes($_GET['Parada']);

		$this->render('admin', array(
			'model' => $model,
			'dataProvider' => $dataProvider,
		));
	}

}