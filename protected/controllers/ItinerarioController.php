<?php

class ItinerarioController extends GxController {
	
	public function accessRules() {
		return array(
			array('allow', 'groups' => array('usuarios')),
			array('deny', 'users' => array('*'))
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Itinerario'),
		));
	}

	public function actionCreate() {
		$model = new Itinerario;


		if (isset($_POST['Itinerario'])) {
			$model->setAttributes($_POST['Itinerario']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id_itinerario));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Itinerario');


		if (isset($_POST['Itinerario'])) {
			$model->setAttributes($_POST['Itinerario']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id_itinerario));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Itinerario')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Itinerario');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Itinerario('search');
		$model->unsetAttributes();

		if (isset($_GET['Itinerario']))
			$model->setAttributes($_GET['Itinerario']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}