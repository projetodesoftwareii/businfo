<?php

class LinhaController extends GxController {

	public function accessRules() {
		return array(
			array('allow', 'groups' => array('usuarios')),
			array('deny', 'users' => array('*'))
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' 		=> $this->loadModel($id, 'Linha'),
			'linha'			=> Linha::model()->with('idParadaInicial')->with('idParadaFinal')->findByPk($id),
			'itinerario' 	=> Itinerario::model()
				->with('paradaIdParada')
				->findAllByAttributes(
					array(
						'linha_id_linha' => $id
					), 
					array(
						'order' => 'ordem'
					)
				),
		));
	}

	public function actionCreate() {
		$model = new Linha;

		if (isset($_POST['Linha'])) {
			$model->setAttributes($_POST['Linha']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id_linha));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Linha');

		if (isset($_POST['Linha'])) {
			$model->setAttributes($_POST['Linha']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id_linha));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Linha')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new Linha('search');
		$model->unsetAttributes();

		if (isset($_GET['Linha']))
			$model->setAttributes($_GET['Linha']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionItinerario($id) {
		$salvo = false; // Flag para mostrar aviso de salvo com sucesso
		if (isset($_POST['Linha'])) {
			// Apaga todos paradas referentes a linha
			Itinerario::model()->deleteAll(
				array(
				    'condition' => "linha_id_linha = :id_linha",
				    'params' 	=> array(':id_linha' => $id),
			    )
			);
			// Adiciona as paradas da linha em ordem
			$itinerario = array();
			foreach ($_POST['Linha']['selectto'] AS $ordem => $id_parada)  {
				$itinerario[$ordem] = new Itinerario;
				$itinerario[$ordem]->linha_id_linha 	= $id;
				$itinerario[$ordem]->parada_id_parada 	= $id_parada;
				$itinerario[$ordem]->ordem 				= $ordem;
				$itinerario[$ordem]->save();
			}
			$salvo = true;
		}

		$model = $this->loadModel($id, 'Linha');
		$linha = Linha::model()->with('idParadaInicial')->with('idParadaFinal')->findByPk($id);
		// Parada inicial e final definidas no cadastro de linha, são consideradas paradas já selecionadas
		$arrayParadasSelecionadas = array(
			$linha->id_parada_inicial,
			$linha->id_parada_final,
		);
		$paradasSelecionadas = Itinerario::model()->with('paradaIdParada')->findAllByAttributes(array('linha_id_linha' => $id), array('order' => 'ordem'));
		foreach ($paradasSelecionadas AS $ps) {
			$arrayParadasSelecionadas[] = $ps->parada_id_parada;
		}
		$paradasNaoSelecionadas = Parada::model()->findAll(
			array(
				'condition' => 'id_parada NOT IN (:paradasJaSelecionadas)',
				'params' 	=> array(
					':paradasJaSelecionadas' => implode(', ', $arrayParadasSelecionadas),
				)
			)
		);
		$this->render('itinerario', array(
			'model'						=> $model,
			'linha' 					=> $linha,
			'paradasSelecionadas' 		=> $paradasSelecionadas,
			'paradasNaoSelecionadas' 	=> $paradasNaoSelecionadas,
			'salvo'						=> $salvo,
		));
	}

	public function actionHorarios($id) {
		$salvo = false; // Flag para mostrar aviso de salvo com sucesso
		if (isset($_POST['Linha'])) {
			// Apaga todos horarios referentes a linha
			Horario::model()->deleteAll(
				array(
				    'condition' => "linha_id_linha = :id_linha",
				    'params' 	=> array(':id_linha' => $id),
			    )
			);
			$horarios = array();
			foreach ($_POST['Linha'] AS $i => $horario) {
				$horarios[$i] = new Horario;
				$horarios[$i]->linha_id_linha	= $id;
				$horarios[$i]->hora 			= $horario['hora'];
				$horarios[$i]->domingo 			= isset($horario['dom']) ? 1 : 0;
				$horarios[$i]->segunda			= isset($horario['seg']) ? 1 : 0;
				$horarios[$i]->terca 			= isset($horario['ter']) ? 1 : 0;
				$horarios[$i]->quarta 			= isset($horario['qua']) ? 1 : 0;
				$horarios[$i]->quinta 			= isset($horario['qui']) ? 1 : 0;
				$horarios[$i]->sexta 			= isset($horario['sex']) ? 1 : 0;
				$horarios[$i]->sabado 			= isset($horario['sab']) ? 1 : 0;
				$horarios[$i]->save();
			}
			$salvo = true;
		}
		$model 		= $this->loadModel($id, 'Linha');
		$linha 		= Linha::model()->with('idParadaInicial')->with('idParadaFinal')->findByPk($id);
		$horarios 	= Horario::model()->findAllByAttributes(array('linha_id_linha' => $id), array('order' => 'hora'));
		$this->render('horarios', array(
				'model' 	=> $model,
				'linha' 	=> $linha,
				'horarios' 	=> $horarios,
				'salvo'		=> $salvo,
			)
		);
	}

}