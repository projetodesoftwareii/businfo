<?php

function cmp($a, $b) {
   return $a['tempo'] - $b['tempo'];
}

class TempoPainelController extends GxController {
	
	public function accessRules() {
		return array(
			array('allow' => array('*'))
		);
	}

	public function actionIndex() {

		$loginInfo = array(
			'username' 	=> isset($_POST['usuario']) ? $_POST['usuario'] : FALSE,
			'password'	=> isset($_POST['senha']) ? $_POST['senha'] : FALSE,
		);
        $modelUser = UserGroupsUser::model()->findByAttributes(array('username' => $loginInfo['username']));
        $senhaSalgada = $modelUser ? md5($loginInfo['password'].$modelUser->getSalt()) : NULL;
        $modelCheckLogin = UserGroupsUser::model()->findByAttributes(array('username' => $loginInfo['username'], 'password' => $senhaSalgada));
        if (!$modelCheckLogin) {
        	$retorno['status']['codigo'] = 0;
			$retorno['status']['mensagem'] = 'Falha na autenticação #1';
			echo json_encode($retorno);
			die();
        }
		// Pega o id do veículo pelo usuário e senha
		$parada = Parada::model()->findByAttributes(array('usergroups_user_id' => $modelCheckLogin->id));
		if (!$parada) {
			$retorno['status']['codigo'] = 0;
			$retorno['status']['mensagem'] = 'Falha na autenticação #2';
			echo json_encode($retorno);
			die();
		}

		// Pega as linhas que iniciam nessa parada
		$horariosParada = array();
		$i = 0;
		$attrHorario = array();
		switch(date('w')) {
			case 0:
			case 7:
				$attrHorario['domingo'] = 1;
				break;
			case 1:
				$attrHorario['segunda'] = 1;
				break;
			case 2:
				$attrHorario['terca'] = 1;
				break;
			case 3:
				$attrHorario['quarta'] = 1;
				break;
			case 4:
				$attrHorario['quinta'] = 1;
				break;
			case 5:
				$attrHorario['sexta'] = 1;
				break;
			case 6:
				$attrHorario['sabado'] = 1;
				break;
		}
		$linhas = Linha::model()->findAllByAttributes(array('id_parada_inicial' => $parada->id_parada));
		foreach ($linhas AS $linha) {
			$linhaId 	= $linha->id_linha;
			$linhaNome 	= $linha->nome;
			$attrHorario['linha_id_linha'] = $linhaId;
			$horarios = Horario::model()->findAllByAttributes($attrHorario, array('condition' => 'hora > :hora', 'params' => array(':hora'=>date('H:i', strtotime('2 hours ago'))), 'order' => 'hora ASC', 'limit' => 2));
			foreach ($horarios AS $horario) {
				$horariosParada[$i]['linha'] = $linhaNome;
				$tempo = self::timeDiffToNow($horario->hora);
				$horariosParada[$i]['tempo'] = $tempo <= 1 ? 'Chegando' : $tempo . 'min';
				$i++;
			}
		}

		// Pega todas as linhas que passam por essa parada
		$sqlLinhas = "SELECT l.id_linha, l.nome FROM linha AS l"
				   . " INNER JOIN itinerario AS i ON (l.id_linha = i.linha_id_linha)"
				   . " WHERE i.parada_id_parada = {$parada->id_parada}";
		foreach (Yii::app()->db->createCommand($sqlLinhas)->queryAll() AS $linha) {
			$linhaId 	= $linha['id_linha'];
			$linhaNome 	= $linha['nome'];
			// Identifica quais veículos iniciaram o percurso dessa linha (máximo 2, ordena por hora de início ascendente)
			$sqlPercursos = "SELECT pi.veiculo_id_veiculo AS id_veiculo FROM percurso_iniciado AS pi"
						  . " INNER JOIN horario AS h ON (pi.horario_id_horario = h.id_horario)"
						  . " INNER JOIN linha AS l ON (h.linha_id_linha = l.id_linha)"
						  . " WHERE l.id_linha='{$linhaId}' AND pi.concluido=0"
						  . " ORDER BY pi.hora"
						  . " LIMIT 2";
			//echo $sqlPercursos . "<br /><br />";
			foreach (Yii::app()->db->createCommand($sqlPercursos)->queryAll() AS $veiculo) {
				// Pega a última posição conhecida de cada veículo
				$sqlLocal = "SELECT localizacao_latitude, localizacao_altitude FROM geolocalizacao WHERE veiculo_id_veiculo={$veiculo['id_veiculo']} ORDER BY timestamp DESC LIMIT 1";
				//echo $sqlLocal . "<br /><br />";
				foreach (Yii::app()->db->createCommand($sqlLocal)->queryAll() AS $local) {
					// Calcula o tempo
					$tempo = self::calcularTempo($local['localizacao_latitude'], $local['localizacao_altitude'], $parada->localizacao_latitude, $parada->localizacao_altitude);
					if ($tempo) {
						$horariosParada[$i]['linha'] = $linhaNome;
						$horariosParada[$i]['tempo'] = $tempo <= 1 ? 'Chegando' : $tempo . 'min';
						$i++;
					}
				}
			}
		}

		$retorno = array();
		$retorno['parada']['id'] = $parada->id_parada;
		$retorno['parada']['nome'] = $parada->nome;
		usort($horariosParada, "cmp");
		$tempo = $horariosParada;
		if (count($tempo) > 0) {
			$retorno['status']['codigo'] = 1;
			$retorno['status']['mensagem'] = 'Listando os dois próximos horários para cada linha que passa pela parada';
			$retorno['parada']['linhas'] = $tempo;
		} else {
			$retorno['status']['codigo'] = 0;
			$retorno['status']['mensagem'] = 'Nenhum ônibus previsto para hoje';
		}

		echo json_encode($retorno);
		die();

	}

	public function calcularTempo($origemLatitude, $origemLongitude, $destinoLatitude, $destinoLongitude) {
		$from = "{$origemLatitude}, {$origemLongitude}";
		$to = "{$destinoLatitude}, {$destinoLongitude}";

		$from = urlencode($from);
		$to = urlencode($to);

		$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?mode=driving&origins=$from&destinations=$to&language=en-EN&sensor=false");
		$data = json_decode($data);
		$aux = reset(reset($data->rows)->elements);
		return str_replace(' mins', '', $aux->duration->text);
	}

	public function timeDiffToNow($horario) {
		$to_time = strtotime(date('Y-m-d') . " {$horario}:00");
		$from_time = strtotime('2 hours ago');
		return floor(abs($to_time - $from_time) / 60);
	}

}