<?php

class VeiculoController extends GxController {

	public function accessRules() {
		return array(
			array('allow', 'groups' => array('usuarios')),
			array('deny', 'users' => array('*'))
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Veiculo'),
		));
	}

	public function actionCreate() {
		$model = new Veiculo;


		if (isset($_POST['Veiculo'])) {
			$model->setAttributes($_POST['Veiculo']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id_veiculo));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Veiculo');


		if (isset($_POST['Veiculo'])) {
			$model->setAttributes($_POST['Veiculo']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id_veiculo));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Veiculo')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$model = new Veiculo();
		$model->unsetAttributes();

		if (isset($_GET['Veiculo']))
			$model->setAttributes($_GET['Veiculo']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}