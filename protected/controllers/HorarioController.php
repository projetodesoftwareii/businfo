<?php

class HorarioController extends GxController {
	
	public function accessRules() {
		return array(
			array('allow', 'groups' => array('usuarios')),
			array('deny', 'users' => array('*'))
		);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Horario'),
		));
	}

	public function actionCreate() {
		$model = new Horario;


		if (isset($_POST['Horario'])) {
			$model->setAttributes($_POST['Horario']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id_horario));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Horario');


		if (isset($_POST['Horario'])) {
			$model->setAttributes($_POST['Horario']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id_horario));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Horario')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Horario');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Horario('search');
		$model->unsetAttributes();

		if (isset($_GET['Horario']))
			$model->setAttributes($_GET['Horario']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdminLinha($id) {
		$model = new Horario('search');
		$model->unsetAttributes();

		if (isset($_GET['Horario']))
			$model->setAttributes($_GET['Horario']);

		$this->render('admin_linha', array(
			'model' => $model,
			'linha_id_linha' => $id,
		));
	}

}