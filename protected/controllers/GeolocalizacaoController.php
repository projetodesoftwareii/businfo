<?php

class GeolocalizacaoController extends GxController {
	
	public function accessRules() {
		return array(
			array('allow' => array('*'))
		);
	}

	public function actionCreate() {

		$loginInfo = array(
			'username' 	=> $_POST['usuario'],
			'password'	=> $_POST['senha'],
		);
        $modelUser = UserGroupsUser::model()->findByAttributes(array('username' => $loginInfo['username']));
        $senhaSalgada = md5($loginInfo['password'].$modelUser->getSalt());
        $modelCheckLogin = UserGroupsUser::model()->findByAttributes(array('username' => $loginInfo['username'], 'password' => $senhaSalgada));
        if (!$modelCheckLogin) {
        	die('Falha na autenticação #1');
        }
		// Pega o id do veículo pelo usuário e senha
		$veiculo = Veiculo::model()->findByAttributes(array('usergroups_user_id' => $modelCheckLogin->id));
		if (!$veiculo) {
			die('Falha na autenticação #2');
		}

		$geolocalizacao = array(
			'localizacao_latitude' 	=> $_POST['latitude'],
			'localizacao_altitude' 	=> $_POST['longitude'],
			'timestamp'			 	=> date('Y-m-d H:i:s'),
			'veiculo_id_veiculo' 	=> $veiculo->id_veiculo,
		);
		$model = new Geolocalizacao;
		$model->setAttributes($geolocalizacao);
		$sucesso = $model->save();
		$this->renderPartial('create', array('model' => $model, 'sucesso' => $sucesso));
	}

}