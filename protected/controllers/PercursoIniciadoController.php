<?php

class PercursoIniciadoController extends GxController {

	public function accessRules() {
		return array(
			array('allow', 'groups' => array('motorista')),
			array('deny', 'users' => array('*'))
		);
	}

	public function actionCreate($id) {
		$model = new PercursoIniciado;
		$model->horario_id_horario = $id;
		$model->data = date('Y-m-d');
		$model->hora = date('H:i:s');
		$model->motorista_id = Yii::app()->user->id;
		$attr = array(
			'linha_id_linha' => $id
		);
		switch (date('w')) {
			case 0:
			case 7:
				$attr['domingo'] = 1;
				break;
			case 1:
				$attr['segunda'] = 1;
				break;
			case 2:
				$attr['terca'] = 1;
				break;
			case 3:
				$attr['quarta'] = 1;
				break;
			case 4:
				$attr['quinta'] = 1;
				break;
			case 5:
				$attr['sexta'] = 1;
				break;
			case 6:
				$attr['sabado'] = 1;
				break;
		}
		$horarios = Horario::model()->findAllByAttributes($attr, 
			array(
				'order' => 'hora'
			)
		);

		$sugestaoHorario = FALSE;
		foreach ($horarios AS $horario) {
			if ($horario->hora >= date('H:i')) {
				$sugestaoHorario = $horario->id_horario;
				break;
			}
		}
		if ($sugestaoHorario) {
			$model->horario_id_horario = $sugestaoHorario;
		}

		if (isset($_POST['PercursoIniciado'])) {
			$model->setAttributes($_POST['PercursoIniciado']);

			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}
		$this->render('create', array('model' => $model, 'horarios' => $horarios));
	}

	public function actionConcluir($id) {
		$model = $this->loadModel($id, 'PercursoIniciado');
		$model->concluido = 1;
		$model->save();
		$this->redirect(array('index'));
	}

	public function actionIndex() {
		$linhasIniciadas = PercursoIniciado::model()->findAllByAttributes(array('motorista_id' => Yii::app()->user->id, 'concluido' => 0));
		if ($linhasIniciadas) {
			$result = array();
			foreach ($linhasIniciadas AS $linhaIniciada) {
				$horarios = Horario::model()->findAllByAttributes(array('id_horario' => $linhaIniciada->horario_id_horario));
				if ($horarios) {
					foreach ($horarios AS $horario) {
						$linha = Linha::model()->findByPk($horario->linha_id_linha);
						if ($linha) {
							$result[] = array(
								'percursoIniciado' 	=> $linhaIniciada->id_veiculo_has_horario,
								'nomeLinha'			=> $linha->nome,
							);
						}
					}
				}
			}
			$this->render('index', array(
				'linhas' => $result,
				'todas'  => FALSE,
			));
		} else {
			$linhas = Linha::model()->findAll();
			$this->render('index', array(
				'linhas' => $linhas,
				'todas'  => TRUE,
			));
		}
	}

}