<?php

class Helpers {
 
    public static function getTabelaHorarios($horarios)
    {
    	$tab = array();
    	foreach ($horarios AS $horario) {
    		$tab[$horario->hora] = array(
    			'dom' => $horario->domingo == 1,
    			'seg' => $horario->segunda == 1,
    			'ter' => $horario->terca   == 1,
    			'qua' => $horario->quarta  == 1,
    			'qui' => $horario->quinta  == 1,
    			'sex' => $horario->sexta   == 1,
    			'sab' => $horario->sabado  == 1,
    		);
    	}
    	ksort($tab);
        return $tab;
    }

}

?>