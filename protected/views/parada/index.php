<?php

$cs = Yii:: app()->getClientScript(); 
$cs->registerScriptFile('https://maps.googleapis.com/maps/api/js?v=3'); 

$this->breadcrumbs = array(
	Parada::label(2),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Criar') . ' ' . Parada::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Atualizar') . ' ' . Parada::label(2), 'url' => array('admin')),
	array('label'=>Yii::t('app', 'Excluir') . ' ' . Parada::label(2), 'url' => array('preDelete')),
);

?>
<h1><?php echo GxHtml::encode(Parada::label(2)); ?></h1>

<?php 

$arrayPontos = array();
foreach ($dataProvider AS $indice => $objetoParada) {
	$arrayPontos[$indice] = array(
		'Id'        => $objetoParada->id_parada,
		"Latitude"  => $objetoParada->localizacao_altitude,
		"Longitude" => $objetoParada->localizacao_latitude,
		"Descricao" => $objetoParada->nome,
	);
}

?>

<script type="text/javascript">
	var infowindow = new Array();
	var marker = new Array();
	function initialize() {
		var mapOptions = {
			zoom: 13,
			center: new google.maps.LatLng(-29.707968, -53.762120)
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		<?php foreach ($dataProvider AS $indice => $objetoParada): ?>
			<?php
				$usuario = "n/d";
				if ($objetoParada->usergroups_user_id != NULL) {
					$objUsuario = UserGroupsUser::model()->findByPk($objetoParada->usergroups_user_id);
					if ($objUsuario) {
						$usuario = $objUsuario->username;
					}
				}
			?>
			marker[<?php echo $indice; ?>] = new google.maps.Marker({
				position: new google.maps.LatLng(<?php echo $objetoParada->localizacao_latitude; ?>,<?php echo $objetoParada->localizacao_altitude; ?>),
				map: map,
				title: '<?php echo $objetoParada->nome; ?>'
			});
			google.maps.event.addListener(marker[<?php echo $indice; ?>], 'click', function() {
				infowindow[<?php echo $indice; ?>] = new google.maps.InfoWindow();
				infowindow[<?php echo $indice; ?>].setContent("<h1><?php echo $objetoParada->nome; ?></h1><p>Localização: (<?php echo $objetoParada->localizacao_latitude; ?>, <?php echo $objetoParada->localizacao_altitude; ?>)<br />Usuário: <?php echo $usuario; ?></p>");
   				infowindow[<?php echo $indice; ?>].open(map,marker[<?php echo $indice; ?>]);
			});
		<?php endforeach; ?>
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div id="map-canvas">Ocorreu um erro, o mapa não pode ser exibido. Verifique a sua conexão com a internet.</div>