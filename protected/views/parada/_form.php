<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'parada-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos com'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'sao obrigatorios'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'nome'); ?>
		<?php echo $form->textField($model, 'nome', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'nome'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'localizacao_latitude'); ?>
		<?php echo $form->textField($model, 'localizacao_latitude'); ?>
		<?php echo $form->error($model,'localizacao_latitude'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'localizacao_altitude'); ?>
		<?php echo $form->textField($model, 'localizacao_altitude'); ?>
		<?php echo $form->error($model,'localizacao_altitude'); ?>
		</div><!-- row -->
		<div class="row">
		<label for="Veiculo_usergroups_user_id">Usuário</label>
		<?php echo $form->dropDownList($model, 'usergroups_user_id', CHtml::listData(UsergroupsUser::model()->findAll(), 'id', 'username'), array('empty' => 'Selecione um usuário')); ?>
		<?php echo $form->error($model,'usergroups_user_id'); ?>
		</div><!-- row -->		

<?php
echo GxHtml::submitButton(Yii::t('app', 'Salvar'));
$this->endWidget();
?>
</div><!-- form -->