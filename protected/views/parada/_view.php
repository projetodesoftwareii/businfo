<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id_parada')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id_parada), array('view', 'id' => $data->id_parada)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nome')); ?>:
	<?php echo GxHtml::encode($data->nome); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('localizacao_latitude')); ?>:
	<?php echo GxHtml::encode($data->localizacao_latitude); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('localizacao_altitude')); ?>:
	<?php echo GxHtml::encode($data->localizacao_altitude); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('usergroups_user_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->usergroupsUser)); ?>
	<br />	

</div>