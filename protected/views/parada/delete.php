<?php

$cs = Yii:: app()->getClientScript(); 
$cs->registerScriptFile('https://maps.googleapis.com/maps/api/js?v=3'); 

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Excluir'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Listar') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Criar') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('parada-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Delete') . ' ' . GxHtml::encode($model->label(2)); ?></h1>


<script>
	function initialize() {
		var mapOptions = {
			zoom: 13,
			center: new google.maps.LatLng(-29.707968, -53.762120)
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		<?php foreach ($dataProvider AS $indice => $objetoParada): ?>
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(<?php echo $objetoParada->localizacao_latitude; ?>,<?php echo $objetoParada->localizacao_altitude; ?>),
				map: map,
				title: '<?php echo $objetoParada->nome; ?>',
				url: 'delete/<?php echo $objetoParada->id_parada; ?>'
			});
			google.maps.event.addListener(marker, 'click', function() {
				window.location.href = this.url;
			});
		<?php endforeach; ?>
	  
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>

<?php if ($salvo) echo "<p class='text-success'>Parada excluída com sucesso!</p>"; ?>

<p>Clique na parada que você deseja excluir</p>
<div id="map-canvas">Ocorreu um erro, o mapa não pode ser exibido. Verifique a sua conexão com a internet.</div>