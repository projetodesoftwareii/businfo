<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id_parada'); ?>
		<?php echo $form->textField($model, 'id_parada'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'nome'); ?>
		<?php echo $form->textField($model, 'nome', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'localizacao_latitude'); ?>
		<?php echo $form->textField($model, 'localizacao_latitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'localizacao_altitude'); ?>
		<?php echo $form->textField($model, 'localizacao_altitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'usergroups_user_id'); ?>
		<?php echo $form->dropDownList($model, 'usergroups_user_id', GxHtml::listDataEx(UsergroupsUser::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>	

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
