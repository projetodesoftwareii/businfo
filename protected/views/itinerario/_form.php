<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'itinerario-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos com'); ?> <span class="required">*</span> <?php echo Yii::t('app', 's�o obrigat�rios'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'linha_id_linha'); ?>
		<?php echo $form->dropDownList($model, 'linha_id_linha', GxHtml::listDataEx(Linha::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'linha_id_linha'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'parada_id_parada'); ?>
		<?php echo $form->dropDownList($model, 'parada_id_parada', GxHtml::listDataEx(Parada::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'parada_id_parada'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ordem'); ?>
		<?php echo $form->textField($model, 'ordem'); ?>
		<?php echo $form->error($model,'ordem'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Salvar'));
$this->endWidget();
?>
</div><!-- form -->