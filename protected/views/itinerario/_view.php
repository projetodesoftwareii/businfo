<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id_itinerario')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id_itinerario), array('view', 'id' => $data->id_itinerario)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('linha_id_linha')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->linhaIdLinha)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('parada_id_parada')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->paradaIdParada)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ordem')); ?>:
	<?php echo GxHtml::encode($data->ordem); ?>
	<br />

</div>