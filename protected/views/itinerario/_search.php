<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id_itinerario'); ?>
		<?php echo $form->textField($model, 'id_itinerario'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'linha_id_linha'); ?>
		<?php echo $form->dropDownList($model, 'linha_id_linha', GxHtml::listDataEx(Linha::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'parada_id_parada'); ?>
		<?php echo $form->dropDownList($model, 'parada_id_parada', GxHtml::listDataEx(Parada::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ordem'); ?>
		<?php echo $form->textField($model, 'ordem'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
