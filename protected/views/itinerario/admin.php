<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Gerenciar'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Listar') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Criar') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('itinerario-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Gerenciar') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<!--<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>--!>

<?php echo GxHtml::link(Yii::t('app', 'Busca Avancada'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'itinerario-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id_itinerario',
		array(
				'name'=>'linha_id_linha',
				'value'=>'GxHtml::valueEx($data->linhaIdLinha)',
				'filter'=>GxHtml::listDataEx(Linha::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'parada_id_parada',
				'value'=>'GxHtml::valueEx($data->paradaIdParada)',
				'filter'=>GxHtml::listDataEx(Parada::model()->findAllAttributes(null, true)),
				),
		'ordem',
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>