<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id_veiculo_has_horario)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id_veiculo_has_horario), 'confirm'=>'Tem certeza que deseja excluir este item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id_veiculo_has_horario',
array(
			'name' => 'veiculoIdVeiculo',
			'type' => 'raw',
			'value' => $model->veiculoIdVeiculo !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->veiculoIdVeiculo)), array('veiculo/view', 'id' => GxActiveRecord::extractPkValue($model->veiculoIdVeiculo, true))) : null,
			),
array(
			'name' => 'horarioIdHorario',
			'type' => 'raw',
			'value' => $model->horarioIdHorario !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->horarioIdHorario)), array('horario/view', 'id' => GxActiveRecord::extractPkValue($model->horarioIdHorario, true))) : null,
			),
'data',
'hora',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('geolocalizacaos')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->geolocalizacaos as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('geolocalizacao/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>