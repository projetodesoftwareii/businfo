<?php

$this->breadcrumbs = array(
	'Meus Percursos',
);
?>

<h1>Meus Percursos</h1>

<?php $baseUrl = Yii::app ()->baseUrl; ?>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl . '/css/gridViewStyle/gridView.css'; ?>" />

<div id="linha-grid" class="grid-view">
	<div class="summary">
	</div>
	<table width="100%" class="items">
		<thead>
			<th>Linha</th>
			<th>Ações</th>
		</thead>
		<tbody>
			<?php foreach ($linhas AS $i => $linha): ?>
				<tr class="<?php echo ($i%2==0) ? 'even' : 'odd'; ?>">
					<td>
						<?php 
							echo $todas ? $linha->nome : $linha['nomeLinha']; 
						?>
					</td>
					<td style="text-align:center">
						<?php if ($todas): ?>
							<a href="<?php echo $baseUrl; ?>/percursoIniciado/create/<?php echo $linha->id_linha; ?>">Iniciar percurso</a>
						<?php else: ?>
							<a href="<?php echo $baseUrl; ?>/percursoIniciado/concluir/<?php echo $linha['percursoIniciado']; ?>">Concluir percurso</a>
						<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>