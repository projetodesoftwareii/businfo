<div class="form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'percurso-iniciado-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos com'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'são obrigatórios'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'veiculo_id_veiculo'); ?>
		<?php echo $form->dropDownList($model, 'veiculo_id_veiculo', GxHtml::listDataEx(Veiculo::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'veiculo_id_veiculo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'horario_id_horario'); ?>
		<?php echo $form->dropDownList($model, 'horario_id_horario', GxHtml::listDataEx($horarios), array('empty' => 'Selecione o Horário')); ?>
		<?php echo $form->error($model,'horario_id_horario'); ?>
		</div><!-- row -->

<?php
echo GxHtml::submitButton(Yii::t('app', 'Iniciar'));
$this->endWidget();
?>
</div><!-- form -->