<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id_veiculo_has_horario')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id_veiculo_has_horario), array('view', 'id' => $data->id_veiculo_has_horario)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('veiculo_id_veiculo')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->veiculoIdVeiculo)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('horario_id_horario')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->horarioIdHorario)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('data')); ?>:
	<?php echo GxHtml::encode($data->data); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('hora')); ?>:
	<?php echo GxHtml::encode($data->hora); ?>
	<br />

</div>