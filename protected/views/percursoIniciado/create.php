<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Iniciar'),
);

$this->menu = array(
	array('label'=>'Voltar', 'url' => array('index')),
);
?>

<h1>Iniciar Percurso</h1>

<?php
$this->renderPartial('_form', array(
		'model' => $model,
		'horarios' => $horarios,
		'buttons' => 'Iniciar'));
?>