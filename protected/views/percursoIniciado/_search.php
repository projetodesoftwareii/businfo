<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id_veiculo_has_horario'); ?>
		<?php echo $form->textField($model, 'id_veiculo_has_horario'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'veiculo_id_veiculo'); ?>
		<?php echo $form->dropDownList($model, 'veiculo_id_veiculo', GxHtml::listDataEx(Veiculo::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'horario_id_horario'); ?>
		<?php echo $form->dropDownList($model, 'horario_id_horario', GxHtml::listDataEx(Horario::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'data'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'data',
			'value' => $model->data,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'hora'); ?>
		<?php echo $form->textField($model, 'hora', array('maxlength' => 8)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
