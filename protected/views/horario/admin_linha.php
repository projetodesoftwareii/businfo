<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Gerenciar'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Listar') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Criar') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('horario-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Gerenciar') . ' ' . GxHtml::encode($model->label(2) . ' de Linha'); ?></h1>

<?php 

$criteria = new CDbCriteria;
$criteria->compare('linha_id_linha', $linha_id_linha);

$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'horario-grid',
		'dataProvider' => $model->createAnotherDataProvider($criteria),
	'columns' => array(
		'id_horario',
		array(
				'name'=>'linha_id_linha',
				'value'=>'GxHtml::valueEx($data->linhaIdLinha)',
				'filter'=>GxHtml::listDataEx(Linha::model()->findAllAttributes(null, true)),
				),
		'hora',
		array(
					'name' => 'domingo',
					'value' => '($data->domingo === 0) ? Yii::t(\'app\', \'Não\') : Yii::t(\'app\', \'Sim\')',
					'filter' => array('0' => Yii::t('app', 'Não'), '1' => Yii::t('app', 'Sim')),
					),
		array(
					'name' => 'segunda',
					'value' => '($data->segunda === 0) ? Yii::t(\'app\', \'Não\') : Yii::t(\'app\', \'Sim\')',
					'filter' => array('0' => Yii::t('app', 'Não'), '1' => Yii::t('app', 'Sim')),
					),
		array(
					'name' => 'terca',
					'value' => '($data->terca === 0) ? Yii::t(\'app\', \'Não\') : Yii::t(\'app\', \'Sim\')',
					'filter' => array('0' => Yii::t('app', 'Não'), '1' => Yii::t('app', 'Sim')),
					),
		array(
					'name' => 'quarta',
					'value' => '($data->quarta === 0) ? Yii::t(\'app\', \'Não\') : Yii::t(\'app\', \'Sim\')',
					'filter' => array('0' => Yii::t('app', 'Não'), '1' => Yii::t('app', 'Sim')),
					),
		array(
					'name' => 'quinta',
					'value' => '($data->quinta === 0) ? Yii::t(\'app\', \'Não\') : Yii::t(\'app\', \'Sim\')',
					'filter' => array('0' => Yii::t('app', 'Não'), '1' => Yii::t('app', 'Sim')),
					),
		array(
					'name' => 'sexta',
					'value' => '($data->sexta === 0) ? Yii::t(\'app\', \'Não\') : Yii::t(\'app\', \'Sim\')',
					'filter' => array('0' => Yii::t('app', 'Não'), '1' => Yii::t('app', 'Sim')),
					),
		array(
					'name' => 'sabado',
					'value' => '($data->sabado === 0) ? Yii::t(\'app\', \'Não\') : Yii::t(\'app\', \'Sim\')',
					'filter' => array('0' => Yii::t('app', 'Não'), '1' => Yii::t('app', 'Sim')),
					),
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>