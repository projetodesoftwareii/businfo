<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'horario-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos com'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'sao obrigatorios'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'linha_id_linha'); ?>
		<?php echo $form->dropDownList($model, 'linha_id_linha', GxHtml::listDataEx(Linha::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'linha_id_linha'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'hora'); ?>
		<?php echo $form->textField($model, 'hora', array('maxlength' => 5)); ?>
		<?php echo $form->error($model,'hora'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'domingo'); ?>
		<?php echo $form->checkBox($model, 'domingo'); ?>
		<?php echo $form->error($model,'domingo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'segunda'); ?>
		<?php echo $form->checkBox($model, 'segunda'); ?>
		<?php echo $form->error($model,'segunda'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'terca'); ?>
		<?php echo $form->checkBox($model, 'terca'); ?>
		<?php echo $form->error($model,'terca'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'quarta'); ?>
		<?php echo $form->checkBox($model, 'quarta'); ?>
		<?php echo $form->error($model,'quarta'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'quinta'); ?>
		<?php echo $form->checkBox($model, 'quinta'); ?>
		<?php echo $form->error($model,'quinta'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sexta'); ?>
		<?php echo $form->checkBox($model, 'sexta'); ?>
		<?php echo $form->error($model,'sexta'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sabado'); ?>
		<?php echo $form->checkBox($model, 'sabado'); ?>
		<?php echo $form->error($model,'sabado'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('percursoIniciados')); ?></label>
		<?php echo $form->checkBoxList($model, 'percursoIniciados', GxHtml::encodeEx(GxHtml::listDataEx(PercursoIniciado::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Salvar'));
$this->endWidget();
?>
</div><!-- form -->