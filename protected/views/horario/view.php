<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Listar') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Criar') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Atualizar') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id_horario)),
	array('label'=>Yii::t('app', 'Excluir') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id_horario), 'confirm'=>'Tem certeza que deseja excluir este item?')),
	array('label'=>Yii::t('app', 'Gerenciar') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id_horario',
array(
			'name' => 'linhaIdLinha',
			'type' => 'raw',
			'value' => $model->linhaIdLinha !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->linhaIdLinha)), array('linha/view', 'id' => GxActiveRecord::extractPkValue($model->linhaIdLinha, true))) : null,
			),
'hora',
'domingo:boolean',
'segunda:boolean',
'terca:boolean',
'quarta:boolean',
'quinta:boolean',
'sexta:boolean',
'sabado:boolean',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('percursoIniciados')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->percursoIniciados as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('percursoIniciado/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>