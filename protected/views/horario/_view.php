<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id_horario')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id_horario), array('view', 'id' => $data->id_horario)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('linha_id_linha')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->linhaIdLinha)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('hora')); ?>:
	<?php echo GxHtml::encode($data->hora); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('domingo')); ?>:
	<?php echo GxHtml::encode($data->domingo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('segunda')); ?>:
	<?php echo GxHtml::encode($data->segunda); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('terca')); ?>:
	<?php echo GxHtml::encode($data->terca); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('quarta')); ?>:
	<?php echo GxHtml::encode($data->quarta); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('quinta')); ?>:
	<?php echo GxHtml::encode($data->quinta); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sexta')); ?>:
	<?php echo GxHtml::encode($data->sexta); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sabado')); ?>:
	<?php echo GxHtml::encode($data->sabado); ?>
	<br />
	*/ ?>

</div>