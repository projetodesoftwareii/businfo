<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id_veiculo'); ?>
		<?php echo $form->textField($model, 'id_veiculo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'prefixo'); ?>
		<?php echo $form->textField($model, 'prefixo', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'fabricante'); ?>
		<?php echo $form->textField($model, 'fabricante', array('maxlength' => 45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ano'); ?>
		<?php echo $form->textField($model, 'ano'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'acessibilidade'); ?>
		<?php echo $form->dropDownList($model, 'acessibilidade', array('0' => Yii::t('app', 'Não'), '1' => Yii::t('app', 'Sim')), array('prompt' => Yii::t('app', 'Todos'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
