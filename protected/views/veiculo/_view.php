<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id_veiculo')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id_veiculo), array('view', 'id' => $data->id_veiculo)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('prefixo')); ?>:
	<?php echo GxHtml::encode($data->prefixo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fabricante')); ?>:
	<?php echo GxHtml::encode($data->fabricante); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ano')); ?>:
	<?php echo GxHtml::encode($data->ano); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('acessibilidade')); ?>:
	<?php echo GxHtml::encode($data->acessibilidade); ?>
	<br />
	<?php echo GxHtml::encode('Usuário'); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->usergroupsUser)); ?>
	<br />
</div>