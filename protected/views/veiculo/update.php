<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Atualizar'),
);

$this->menu = array(
	array('label' => Yii::t('app', 'Listar') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label' => Yii::t('app', 'Criar') . ' ' . $model->label(), 'url'=>array('create')),
	array('label' => Yii::t('app', 'Visualizar') . ' ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
);
?>

<h1><?php echo Yii::t('app', 'Atualizando') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->renderPartial('_form', array(
		'model' => $model));
?>