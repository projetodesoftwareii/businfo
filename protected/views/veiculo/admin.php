<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Gerenciar'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Criar') . ' ' . $model->label(), 'url'=>array('create')),
	);

?>

<h1><?php echo Yii::t('app', 'Gerenciando') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'veiculo-grid',
	'dataProvider' => $model->search(),
	//'dataProvider' => $dataProvider,
	'cssFile' => Yii::app()->baseUrl . '/css/gridViewStyle/gridView.css',
	'columns' => array(
		'id_veiculo',
		'prefixo',
		'fabricante',
		'ano',
		array(
					'name' => 'acessibilidade',
					'value' => '($data->acessibilidade === 0) ? Yii::t(\'app\', \'Não\') : Yii::t(\'app\', \'Sim\')',
					'filter' => array('0' => Yii::t('app', 'Não'), '1' => Yii::t('app', 'Sim')),
					),
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>