<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'veiculo-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos com'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'sao obrigatorios'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'prefixo'); ?>
		<?php echo $form->textField($model, 'prefixo', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'prefixo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fabricante'); ?>
		<?php echo $form->textField($model, 'fabricante', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'fabricante'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ano'); ?>
		<?php echo $form->textField($model, 'ano'); ?>
		<?php echo $form->error($model,'ano'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'acessibilidade'); ?>
		<?php echo $form->checkBox($model, 'acessibilidade'); ?>
		<?php echo $form->error($model,'acessibilidade'); ?>
		</div><!-- row -->
		<div class="row">
		<label for="Veiculo_usergroups_user_id">Usuário</label>
		<?php echo $form->dropDownList($model, 'usergroups_user_id', CHtml::listData(UsergroupsUser::model()->findAll(), 'id', 'username'), array('empty' => 'Selecione um usuário')); ?>
		<?php echo $form->error($model,'usergroups_user_id'); ?>
		</div><!-- row -->

<?php
echo GxHtml::submitButton(Yii::t('app', 'Salvar'));
$this->endWidget();
?>
</div><!-- form -->