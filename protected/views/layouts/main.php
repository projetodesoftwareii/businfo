<?php /* @var $this Controller */ ?>
<!DOCTYPE>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<?php echo Yii::app()->bootstrap->register();?>

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php 
		$this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index'), 'visible'=>(!Yii::app()->user->isGuest && in_array(Yii::app()->user->group, array(1,2)))),
				array('label'=>'Paradas', 'url'=>array('/parada'), 'visible'=>(!Yii::app()->user->isGuest && in_array(Yii::app()->user->group, array(1,2)))),
				array('label'=>'Linhas', 'url'=>array('/linha'), 'visible'=>(!Yii::app()->user->isGuest && in_array(Yii::app()->user->group, array(1,2)))),
				array('label'=>'Veículos', 'url'=>array('/veiculo'), 'visible'=>(!Yii::app()->user->isGuest && in_array(Yii::app()->user->group, array(1,2)))),
				array('label'=>'Usuários', 'url'=>array('/userGroups'), 'visible'=>(!Yii::app()->user->isGuest && in_array(Yii::app()->user->group, array(1)))),
				array('label'=>'Iniciar percurso', 'url'=>array('/percursoIniciado'), 'visible'=>(!Yii::app()->user->isGuest && in_array(Yii::app()->user->group, array(3)))),
				array('label'=>'Login', 'url'=>array('/userGroups'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/userGroups/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		<?php $baseUrl = Yii::app ()->baseUrl; ?>
		<img src="<?php echo $baseUrl; ?>/assets/a8f0f84/img/logo1.png" /><br />
		<?php echo date('Y'); ?> by <a href="mailto:ldutra@inf.ufsm.br">Lorena Dutra</a> e <a href="mailto:mburkard@inf.ufsm.br">Marcelo Burkard</a>.<br />
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
