<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'linha-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos com'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'sao obrigatorios'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'nome'); ?>
		<?php echo $form->textField($model, 'nome', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'nome'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'id_parada_inicial'); ?>
		<?php echo $form->dropDownList($model, 'id_parada_inicial', GxHtml::listDataEx(Parada::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'id_parada_inicial'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'id_parada_final'); ?>
		<?php echo $form->dropDownList($model, 'id_parada_final', GxHtml::listDataEx(Parada::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'id_parada_final'); ?>
		</div><!-- row -->

<?php
echo GxHtml::submitButton(Yii::t('app', 'Salvar'));
$this->endWidget();
?>
</div><!-- form -->