<?php

$cs = Yii:: app()->getClientScript(); 
$cs->registerScriptFile('https://maps.googleapis.com/maps/api/js?v=3'); 

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Listar') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Criar') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Atualizar') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id_linha)),
	array('label'=>Yii::t('app', 'Excluir') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id_linha), 'confirm'=>'Tem certeza que deseja excluir este item?')),
	array('label'=>Yii::t('app', 'Gerenciar') . ' horários da ' . $model->label(1), 'url'=>array('horarios', 'id' => $model->id_linha)),
	array('label'=>Yii::t('app', 'Gerenciar') . ' itinerário da ' . $model->label(1), 'url'=>array('itinerario', 'id' => $model->id_linha)),
);
?>

<h1><?php echo Yii::t('app', 'Visualizando') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id_linha',
'nome',
array(
			'name' => 'idParadaInicial',
			'type' => 'raw',
			'value' => $model->idParadaInicial !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idParadaInicial)), array('parada/view', 'id' => GxActiveRecord::extractPkValue($model->idParadaInicial, true))) : null,
			),
array(
			'name' => 'idParadaFinal',
			'type' => 'raw',
			'value' => $model->idParadaFinal !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idParadaFinal)), array('parada/view', 'id' => GxActiveRecord::extractPkValue($model->idParadaFinal, true))) : null,
			),
	),
)); ?>

<h2>Itinerário</h2>
<p>* Limite de 7 paradas para a versão grátis da API</p>
<?php
	$waypointsJS = array();
	foreach ($itinerario AS $parada) {
		$waypointsJS[] = "{location: new google.maps.LatLng({$parada->paradaIdParada->localizacao_latitude},{$parada->paradaIdParada->localizacao_altitude})}";
	}
	$waypointsJS = array_slice($waypointsJS, 0, 7);
?>

<div id="map-canvas"></div>

<script>
	var map;
	var directionsDisplay; // Instanciaremos ele mais tarde, que será o nosso google.maps.DirectionsRenderer
	var directionsService = new google.maps.DirectionsService();
	 
	function initialize() {
	   directionsDisplay = new google.maps.DirectionsRenderer(); // Instanciando...
	   var options = {
	      zoom: 5,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	   };
	   map = new google.maps.Map(document.getElementById("map-canvas"), options);
	   directionsDisplay.setMap(map); // Relacionamos o directionsDisplay com o mapa desejado
	}
	 
	initialize();

	var request = { // Novo objeto google.maps.DirectionsRequest, contendo:
		origin: new google.maps.LatLng(<?php echo $linha->idParadaInicial->localizacao_latitude; ?>,<?php echo $linha->idParadaInicial->localizacao_altitude ?>),
		destination: new google.maps.LatLng(<?php echo $linha->idParadaFinal->localizacao_latitude; ?>,<?php echo $linha->idParadaFinal->localizacao_altitude ?>),
		waypoints: [<?php echo implode(',', $waypointsJS); ?>],
		travelMode: google.maps.TravelMode.DRIVING // meio de transporte, nesse caso, de carro
	};

	directionsService.route(request, function(result, status) {
	if (status == google.maps.DirectionsStatus.OK) { // Se deu tudo certo
		directionsDisplay.setDirections(result); // Renderizamos no mapa o resultado
	}
	});
</script>

<h2><?php echo GxHtml::encode($model->getRelationLabel('horarios')); ?></h2>

<table>
	<thead>
		<th>Horário</th>
		<th>Domingo</th>
		<th>Segunda</th>
		<th>Terça</th>
		<th>Quarta</th>
		<th>Quinta</th>
		<th>Sexta</th>
		<th>Sábado</th>
	</thead>
	<tbody>
		<?php foreach (Helpers::getTabelaHorarios($model->horarios) AS $hora => $dias): ?>
			<tr>
				<td><?php echo $hora; ?></td>
				<td><?php if ($dias['dom']) echo "X"; ?></td>
				<td><?php if ($dias['seg']) echo "X"; ?></td>
				<td><?php if ($dias['ter']) echo "X"; ?></td>
				<td><?php if ($dias['qua']) echo "X"; ?></td>
				<td><?php if ($dias['qui']) echo "X"; ?></td>
				<td><?php if ($dias['sex']) echo "X"; ?></td>
				<td><?php if ($dias['sab']) echo "X"; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>