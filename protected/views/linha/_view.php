<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id_linha')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id_linha), array('view', 'id' => $data->id_linha)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nome')); ?>:
	<?php echo GxHtml::encode($data->nome); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('id_parada_inicial')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idParadaInicial)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('id_parada_final')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idParadaFinal)); ?>
	<br />

</div>