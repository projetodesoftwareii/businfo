<?php

$this->breadcrumbs = array(
	Linha::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Criar') . ' ' . Linha::label(), 'url' => array('create')),
);
?>

<h1><?php echo GxHtml::encode(Linha::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 