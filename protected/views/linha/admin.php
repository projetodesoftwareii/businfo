<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Gerenciar'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Criar') . ' ' . $model->label(), 'url'=>array('create')),
	);

?>

<h1><?php echo Yii::t('app', 'Gerenciando') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'linha-grid',
	'dataProvider' => $model->search(),
	'cssFile' => Yii::app()->baseUrl . '/css/gridViewStyle/gridView.css',
	'columns' => array(
		'id_linha',
		'nome',
		array(
				'name'=>'id_parada_inicial',
				'value'=>'GxHtml::valueEx($data->idParadaInicial)',
				'filter'=>GxHtml::listDataEx(Parada::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'id_parada_final',
				'value'=>'GxHtml::valueEx($data->idParadaFinal)',
				'filter'=>GxHtml::listDataEx(Parada::model()->findAllAttributes(null, true)),
				),
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>