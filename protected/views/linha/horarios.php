<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model) => array('view', 'id' => $model->id_linha),
	'Horários',
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Gerenciar') . ' ' . $model->label(1), 'url'=>array('view', 'id' => $model->id_linha)),
	array('label'=>Yii::t('app', 'Gerenciar') . ' itinerário da ' . $model->label(1), 'url'=>array('itinerario', 'id' => $model->id_linha)),
);
?>

<h1><?php echo Yii::t('app', 'Gerenciar horários') . ' ' . $linha->nome; ?></h1>

<style>
    .form table tbody tr td input[type=text] {
    	width: 51px;
    	height: 30px;
    }
</style>

<div class="form">

<?php 
$form = $this->beginWidget(
	'GxActiveForm', 
	array(
    	'id' => 'linha-form',
    	'enableAjaxValidation' => false,
	)
);
?>
	<?php if ($salvo) echo "<p class='text-success'>Alterações salvas com sucesso!</p>"; ?>
	<table id="tabela-horarios">
		<thead>
			<th>Hora</th>
			<th>Domingo</th>
			<th>Segunda</th>
			<th>Terça</th>
			<th>Quarta</th>
			<th>Quinta</th>
			<th>Sexta</th>
			<th>Sábado</th>
			<th></th>
		</thead>
		<tbody>
		<?php $i = 0; ?>
		<?php foreach ($horarios AS $horario): ?>
			<tr>
				<td><input type="text" name="Linha[<?php echo $i; ?>][hora]" class="hora" value="<?php echo $horario->hora; ?>" /></td>
				<td><input type="checkbox" name="Linha[<?php echo $i; ?>][dom]" <?php if ($horario->domingo == 1) echo "checked"; ?> /></td>
				<td><input type="checkbox" name="Linha[<?php echo $i; ?>][seg]" <?php if ($horario->segunda == 1) echo "checked"; ?> /></td>
				<td><input type="checkbox" name="Linha[<?php echo $i; ?>][ter]" <?php if ($horario->terca == 1)   echo "checked"; ?> /></td>
				<td><input type="checkbox" name="Linha[<?php echo $i; ?>][qua]" <?php if ($horario->quarta == 1)  echo "checked"; ?> /></td>
				<td><input type="checkbox" name="Linha[<?php echo $i; ?>][qui]" <?php if ($horario->quinta == 1)  echo "checked"; ?> /></td>
				<td><input type="checkbox" name="Linha[<?php echo $i; ?>][sex]" <?php if ($horario->sexta == 1)   echo "checked"; ?> /></td>
				<td><input type="checkbox" name="Linha[<?php echo $i; ?>][sab]" <?php if ($horario->sabado == 1)  echo "checked"; ?> /></td>
				<td><a class="btn-delete-horario">Remover</a></td>
			</tr>
			<?php $i++; ?>
		<?php endforeach; ?>
		</tbody>
	</table>

	<p><a id="btn-add-horario">Adicionar Horário</a></p>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Salvar'));
$this->endWidget();
?>
</div><!-- form -->

<?php
  $baseUrl = Yii::app()->baseUrl;  
  $cs = Yii::app()->getClientScript(); 
  /*$cs->registerScriptFile($baseUrl . 'js/infobox.js'); 
  $cs->registerScriptFile($baseUrl . 'js/markerclusterer.js');
  $cs->registerScriptFile($baseUrl . 'js/mapa.js');
  $cs->registerScriptFile($baseUrl . 'js/jquery.min.js');
  $cs->registerScriptFile($baseUrl . 'js/js.js');*/
  $cs->registerScriptFile('http://keith-wood.name/js/jquery.plugin.js');
  $cs->registerScriptFile('http://keith-wood.name/js/jquery.timeentry.js');
?>

<script>
$(document).ready(function() {
	var i = <?php echo $i; ?>;
	$('.hora').timeEntry({show24Hours: true});
	$('#btn-add-horario').on('click', function(e){
		e.preventDefault();
		$('#tabela-horarios tbody').append('<tr><td><input type="text" name="Linha[' + i + '][hora]" class="hora" /></td><td><input type="checkbox" name="Linha[' + i + '][dom]" /></td><td><input type="checkbox" name="Linha[' + i + '][seg]" /></td><td><input type="checkbox" name="Linha[' + i + '][ter]" /></td><td><input type="checkbox" name="Linha[' + i + '][qua]" /></td><td><input type="checkbox" name="Linha[' + i + '][qui]" /></td><td><input type="checkbox" name="Linha[' + i + '][sex]" /></td><td><input type="checkbox" name="Linha[' + i + '][sab]" /></td><td><a class="btn-delete-horario">Remover</a></td></tr>');
		$('.hora').timeEntry({show24Hours: true});
		i++;
	});

	$('#tabela-horarios').on('click', 'a.btn-delete-horario', function(e){
		e.preventDefault();
		$(this).parent().parent().css('background-color', 'red');
		$(this).parent().parent().fadeOut(300, function(){
			$(this).remove();
		});
	});
});
</script>