<?php

$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => $model->id_linha),
    'Itinerário'
);

$this->menu=array(
    array('label'=>Yii::t('app', 'Gerenciar') . ' ' . $model->label(1), 'url'=>array('view', 'id' => $model->id_linha)),
    array('label'=>Yii::t('app', 'Gerenciar') . ' horários da ' . $model->label(1), 'url'=>array('horarios', 'id' => $model->id_linha)),
);
?>

<h1><?php echo Yii::t('app', 'Gerenciar itinerário') . ' ' . $linha->nome; ?></h1>

<style>
    .bloco {
        display:block;
        float:left;
    }
</style>

<div class="form">

<?php $form = $this->beginWidget('GxActiveForm', array(
    'id' => 'linha-form',
    'enableAjaxValidation' => false,
));
?>

	<fieldset>
        <?php if ($salvo) echo "<p class='text-success'>Alterações salvas com sucesso!</p>"; ?>
        <div style="width:35%;" class="bloco">
            <p>Todas paradas</p>
    		<select name="selectfrom[]" id="select-from" multiple size="8">
    			<?php foreach ($paradasNaoSelecionadas AS $parada): ?>
    				<option value="<?php echo $parada->id_parada; ?>"><?php echo $parada->nome; ?></option>
    			<?php endforeach; ?>
    		</select>
        </div>
        <div style="width:15%;" class="bloco">
            <p>Adicione ou remova paradas</p>
    		<a href="JavaScript:void(0);" id="btn-add">Adicionar &raquo;</a><br />
    		<a href="JavaScript:void(0);" id="btn-remove">&laquo; Remover</a>
        </div>
        <div style="width:35%;" class="bloco">
            <p>Itinerário</p>
    		Início: <?php echo $linha->idParadaInicial->nome; ?>
    		<select name="Linha[selectto][]" id="select-to" multiple size="5">
    			<?php foreach ($paradasSelecionadas AS $parada): ?>
    				<option value="<?php echo $parada->paradaIdParada->id_parada; ?>"><?php echo $parada->paradaIdParada->nome; ?></option>
    			<?php endforeach; ?>
    		</select>
            Término: <?php echo $linha->idParadaFinal->nome; ?>
        </div>
        <div style="width:15%;" class="bloco">
            <p>Ordene as paradas</p>
    		<a href="JavaScript:void(0);" id="btn-up">Antes</a><br />
    		<a href="JavaScript:void(0);" id="btn-down">Depois</a>
        </div>
	</fieldset>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Salvar'));
$this->endWidget();
?>
</div><!-- form -->

<script>

$(document).ready(function() {
    // Seleciona os itens antes de dar submit
    $('#linha-form').submit(function(){
        var paradas = $('#select-to option');
        for (var i=0; i<paradas.length; i++) {
            paradas[i].selected = true;
        }
    });

    $('#btn-add').click(function(){
        $('#select-from option:selected').each( function() {
                $('#select-to').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
    });
    $('#btn-remove').click(function(){
        $('#select-to option:selected').each( function() {
            $('#select-from').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
        });
    });
    $('#btn-up').bind('click', function() {
        $('#select-to option:selected').each( function() {
            var newPos = $('#select-to option').index(this) - 1;
            if (newPos > -1) {
                $('#select-to option').eq(newPos).before("<option value='"+$(this).val()+"' selected='selected'>"+$(this).text()+"</option>");
                $(this).remove();
            }
        });
    });
    $('#btn-down').bind('click', function() {
        var countOptions = $('#select-to option').size();
        $('#select-to option:selected').each( function() {
            var newPos = $('#select-to option').index(this) + 1;
            if (newPos < countOptions) {
                $('#select-to option').eq(newPos).after("<option value='"+$(this).val()+"' selected='selected'>"+$(this).text()+"</option>");
                $(this).remove();
            }
        });
    });
});

</script>