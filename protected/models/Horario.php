<?php

Yii::import('application.models._base.BaseHorario');

class Horario extends BaseHorario
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function createAnotherDataProvider($criteria) {
	    return new CActiveDataProvider('Horario', array(
	        'criteria' => $criteria,
	    ));
	}	
}