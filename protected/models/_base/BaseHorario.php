<?php

/**
 * This is the model base class for the table "horario".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Horario".
 *
 * Columns in table "horario" available as properties of the model,
 * followed by relations of table "horario" available as properties of the model.
 *
 * @property integer $id_horario
 * @property integer $linha_id_linha
 * @property string $hora
 * @property integer $domingo
 * @property integer $segunda
 * @property integer $terca
 * @property integer $quarta
 * @property integer $quinta
 * @property integer $sexta
 * @property integer $sabado
 *
 * @property Linha $linhaIdLinha
 * @property PercursoIniciado[] $percursoIniciados
 */
abstract class BaseHorario extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'horario';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Horário|Horários', $n);
	}

	public static function representingColumn() {
		return 'hora';
	}

	public function rules() {
		return array(
			array('linha_id_linha, hora', 'required'),
			array('linha_id_linha, domingo, segunda, terca, quarta, quinta, sexta, sabado', 'numerical', 'integerOnly'=>true),
			array('hora', 'length', 'max'=>5),
			array('domingo, segunda, terca, quarta, quinta, sexta, sabado', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id_horario, linha_id_linha, hora, domingo, segunda, terca, quarta, quinta, sexta, sabado', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'linhaIdLinha' => array(self::BELONGS_TO, 'Linha', 'linha_id_linha'),
			'percursoIniciados' => array(self::HAS_MANY, 'PercursoIniciado', 'horario_id_horario'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id_horario' => Yii::t('app', '# Horário'),
			'linha_id_linha' => null,
			'hora' => Yii::t('app', 'Hora'),
			'domingo' => Yii::t('app', 'Domingo'),
			'segunda' => Yii::t('app', 'Segunda-Feira'),
			'terca' => Yii::t('app', 'Terça-Feira'),
			'quarta' => Yii::t('app', 'Quarta-Feira'),
			'quinta' => Yii::t('app', 'Quinta-Feira'),
			'sexta' => Yii::t('app', 'Sexta-Feira'),
			'sabado' => Yii::t('app', 'Sábado'),
			'linhaIdLinha' => null,
			'percursoIniciados' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id_horario', $this->id_horario);
		$criteria->compare('linha_id_linha', $this->linha_id_linha);
		$criteria->compare('hora', $this->hora, true);
		$criteria->compare('domingo', $this->domingo);
		$criteria->compare('segunda', $this->segunda);
		$criteria->compare('terca', $this->terca);
		$criteria->compare('quarta', $this->quarta);
		$criteria->compare('quinta', $this->quinta);
		$criteria->compare('sexta', $this->sexta);
		$criteria->compare('sabado', $this->sabado);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}