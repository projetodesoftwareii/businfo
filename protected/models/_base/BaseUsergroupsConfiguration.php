<?php

/**
 * This is the model base class for the table "usergroups_configuration".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "UsergroupsConfiguration".
 *
 * Columns in table "usergroups_configuration" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $rule
 * @property string $value
 * @property string $options
 * @property string $description
 *
 */
abstract class BaseUsergroupsConfiguration extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'usergroups_configuration';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'UsergroupsConfiguration|UsergroupsConfigurations', $n);
	}

	public static function representingColumn() {
		return 'rule';
	}

	public function rules() {
		return array(
			array('rule', 'length', 'max'=>40),
			array('value', 'length', 'max'=>20),
			array('options, description', 'safe'),
			array('rule, value, options, description', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, rule, value, options, description', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'rule' => Yii::t('app', 'Rule'),
			'value' => Yii::t('app', 'Value'),
			'options' => Yii::t('app', 'Options'),
			'description' => Yii::t('app', 'Description'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('rule', $this->rule, true);
		$criteria->compare('value', $this->value, true);
		$criteria->compare('options', $this->options, true);
		$criteria->compare('description', $this->description, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}