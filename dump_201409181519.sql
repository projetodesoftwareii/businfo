SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `businfo`.`linha` (
  `id_linha` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `id_parada_inicial` INT(11) NOT NULL,
  `id_parada_final` INT(11) NOT NULL,
  PRIMARY KEY (`id_linha`),
  INDEX `fk_linha_parada1_idx` (`id_parada_inicial` ASC),
  INDEX `fk_linha_parada2_idx` (`id_parada_final` ASC),
  CONSTRAINT `fk_linha_parada1`
    FOREIGN KEY (`id_parada_inicial`)
    REFERENCES `businfo`.`parada` (`id_parada`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_linha_parada2`
    FOREIGN KEY (`id_parada_final`)
    REFERENCES `businfo`.`parada` (`id_parada`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `businfo`.`itinerario` (
  `id_itinerario` INT(11) NOT NULL AUTO_INCREMENT,
  `linha_id_linha` INT(11) NOT NULL,
  `parada_id_parada` INT(11) NOT NULL,
  `ordem` INT(11) NOT NULL,
  PRIMARY KEY (`id_itinerario`),
  INDEX `fk_linha_has_parada_parada1_idx` (`parada_id_parada` ASC),
  INDEX `fk_linha_has_parada_linha_idx` (`linha_id_linha` ASC),
  CONSTRAINT `fk_linha_has_parada_linha`
    FOREIGN KEY (`linha_id_linha`)
    REFERENCES `businfo`.`linha` (`id_linha`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_linha_has_parada_parada1`
    FOREIGN KEY (`parada_id_parada`)
    REFERENCES `businfo`.`parada` (`id_parada`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `businfo`.`horario` (
  `id_horario` INT(11) NOT NULL AUTO_INCREMENT,
  `linha_id_linha` INT(11) NOT NULL,
  `hora` VARCHAR(5) NOT NULL,
  `domingo` TINYINT(1) NOT NULL DEFAULT 0,
  `segunda` TINYINT(1) NOT NULL DEFAULT 0,
  `terca` TINYINT(1) NOT NULL DEFAULT 0,
  `quarta` TINYINT(1) NOT NULL DEFAULT 0,
  `quinta` TINYINT(1) NOT NULL DEFAULT 0,
  `sexta` TINYINT(1) NOT NULL DEFAULT 0,
  `sabado` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_horario`),
  INDEX `fk_horario_linha1_idx` (`linha_id_linha` ASC),
  CONSTRAINT `fk_horario_linha1`
    FOREIGN KEY (`linha_id_linha`)
    REFERENCES `businfo`.`linha` (`id_linha`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `businfo`.`veiculo` (
  `id_veiculo` INT(11) NOT NULL AUTO_INCREMENT,
  `prefixo` VARCHAR(45) NOT NULL,
  `fabricante` VARCHAR(45) NULL DEFAULT NULL,
  `ano` INT(11) NULL DEFAULT NULL,
  `acessibilidade` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_veiculo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `businfo`.`percurso_iniciado` (
  `id_veiculo_has_horario` INT(11) NOT NULL AUTO_INCREMENT,
  `veiculo_id_veiculo` INT(11) NOT NULL,
  `horario_id_horario` INT(11) NOT NULL,
  `data` DATE NOT NULL,
  `hora` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`id_veiculo_has_horario`),
  INDEX `fk_veiculo_has_horario_horario1_idx` (`horario_id_horario` ASC),
  INDEX `fk_veiculo_has_horario_veiculo1_idx` (`veiculo_id_veiculo` ASC),
  CONSTRAINT `fk_veiculo_has_horario_veiculo1`
    FOREIGN KEY (`veiculo_id_veiculo`)
    REFERENCES `businfo`.`veiculo` (`id_veiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_veiculo_has_horario_horario1`
    FOREIGN KEY (`horario_id_horario`)
    REFERENCES `businfo`.`horario` (`id_horario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `businfo`.`geolocalizacao` (
  `id_geolocalizacao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_percurso_iniciado` INT(11) NOT NULL,
  `timestamp` TIMESTAMP NOT NULL,
  `localizacao_latitude` FLOAT(11) NOT NULL,
  `localizacao_altitude` FLOAT(11) NOT NULL,
  PRIMARY KEY (`id_geolocalizacao`),
  INDEX `fk_localizacao_veiculo_has_horario1_idx` (`id_percurso_iniciado` ASC),
  CONSTRAINT `fk_localizacao_veiculo_has_horario1`
    FOREIGN KEY (`id_percurso_iniciado`)
    REFERENCES `businfo`.`percurso_iniciado` (`id_veiculo_has_horario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
